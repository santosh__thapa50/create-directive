import { Component, OnInit } from '@angular/core';
import { timer } from 'rxjs';
import { Time } from '@angular/common';
import { toDate } from '@angular/common/src/i18n/format_date';
import * as moment from 'moment';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {
  subscribeTimer: any;
  time:Date;
  timeLeft: any = null;
  constructor() { }

  ngOnInit() {
  }

  format(ms) {
    const timeDate = new Date();
    timeDate.setHours( Math.floor((ms % (60 * 60 * 24)) / (60 * 60)));
    timeDate.setMinutes( Math.floor(((ms % (60 * 60 * 24)) % (60 * 60)) / 60));
    timeDate.setSeconds( Math.floor(((ms % (60 * 60 * 24)) % (60 * 60)) % 60));
   return timeDate;
  }
  
  oberserableTimer() {
    const source = timer(1000, 2000);
    const abc = source.subscribe(val => {
      this.time= this.format(val);
      console.log(this.time);
      this.subscribeTimer = this.timeLeft - val;
    });
  }

}
