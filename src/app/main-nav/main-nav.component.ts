import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import * as $ from 'jquery';
@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
    private router: Router) {
  }
 
  public ngOnInit() {
    $('#sidebarCollapse').on('click', function () {
      if (('#sidebarCollapse').includes('sidebar .active')) {
        $('#sidebar .active').removeClass('active');
      } else {
        $('#sidebar').toggleClass('active');
      }
    });
  }

  Logout() {
    this.router.navigate(['']);
  }

}
