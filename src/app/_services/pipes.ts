import {Pipe , PipeTransform } from "@angular/core";

/**
 * A simple string filter, since Angular does not yet have a filter pipe built in.
 */
@Pipe({
    name: 'stringFilter'
})
export class StringFilterPipe implements PipeTransform  {

    transform(value: string[], q: string) {
        if (!q || q === '') {
            return value;
        }
        return value.filter(item => -1 < item.toLowerCase().indexOf(q.toLowerCase()));
    }
}

@Pipe({ name: 'filter' })
export class FilterArrayPipe implements PipeTransform {
//   transform(items: any[], searchText: string): any[] {
//    console.log("items" + JSON.stringify(items) + " Search Text" + searchText);
//     if(!items) return [];
//     if(!searchText) return items;
// searchText = searchText.toLowerCase();
// return items.filter( it => {
//   console.log(JSON.stringify(it))
//       return it.toString().toLowerCase().includes(searchText);
//     });
//    }
  
  transform(value, args) {
   
   console.log("value :" + JSON.stringify(value) + "Args :" + args);
    if (!args[0]) {
      return value;
    } else if (value) {
      return value.filter(item => {
        for (let key in item) {
          if ((typeof item[key] === 'string' || item[key] instanceof String) && 
              (item[key].indexOf(args[0]) !== -1)) {
            return true;
          }
        }
      });
    }
  }
}