import { Directive, Output, EventEmitter, OnInit, HostListener, Input } from '@angular/core';
import { SortService } from 'src/app/sortable-table/sort.service';
import { Subscription, Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';



@Directive({
  selector: '[sortable-table]'
})

export class SortableDirective implements OnInit {

  constructor(private sortService: SortService) { }

  @Input()
  sorted = new EventEmitter();

  @Output() debounceClick = new EventEmitter();
  private clicks = new Subject();

  private columnSortedSubscription: Subscription;

  ngOnInit() {
    console.log("directive");
    // subscribe to sort changes so we emit and event for this data table
     this.columnSortedSubscription = this.sortService.columnSorted$.subscribe(event => {
            this.sorted.emit(event);
        });
  }

 

  ngOnDestroy() {
    this.columnSortedSubscription.unsubscribe();
  }

}
