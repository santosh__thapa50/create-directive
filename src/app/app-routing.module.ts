import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { TimerComponent } from './timer/timer.component';
import { AttendenceComponent } from './attendence/attendence.component';
import { AboutusComponent } from './aboutus/aboutus.component';

const routes: Routes = [
  {path: '' ,component: LoginComponent},
  {path : 'main-menu' ,component: MainNavComponent,
  children:[
    {path: '' ,component:DashboardComponent},
    {path: 'Dashboard' ,component:DashboardComponent},
    {path: 'timer' ,component: TimerComponent},
    {path:'attendnece',component: AttendenceComponent},
    {path:'aboutUs' ,component: AboutusComponent}

  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
