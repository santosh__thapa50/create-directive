import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule ,NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS ,HttpClientJsonpModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { NgxPaginationModule } from 'ngx-pagination';
import { StringFilterPipe, FilterArrayPipe } from './_services';
import { SortService } from './sortable-table/sort.service';
import { SortableTableDirective } from './sortable-table/sortable-table.directive';
import { SortableColumnComponent } from './sortable-table/sortable-column.component';
import { SortableDirective } from './_services/directives/sortable.directive';
import { TimerComponent } from './timer/timer.component';
import { AttendenceComponent } from './attendence/attendence.component';
import * as moment from 'moment';
import * as $ from 'jquery';
import { AboutusComponent } from './aboutus/aboutus.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    MainNavComponent,
    StringFilterPipe,
    SortableColumnComponent,
    SortableDirective,
    TimerComponent,
    AttendenceComponent,
    AboutusComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    LayoutModule,
    NgxPaginationModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    

    
  ],
  providers: [ SortService],
  bootstrap: [AppComponent]
})
export class AppModule { }
